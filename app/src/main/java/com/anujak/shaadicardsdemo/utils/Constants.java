package com.anujak.shaadicardsdemo.utils;

public class Constants {
    public static String BASE_URL= "https://randomuser.me/";
    public interface FONTS{
        String BUTTON_FONT = "roboto_bold_0.ttf";
        String APP_FONT = "roboto_regular.ttf";
    }
}
