package com.anujak.shaadicardsdemo.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.anujak.shaadicardsdemo.utils.FontCache;

import static com.anujak.shaadicardsdemo.utils.Constants.FONTS.APP_FONT;


@SuppressLint("AppCompatCustomView")
public class AppTextview extends TextView {

    public AppTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AppTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AppTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = FontCache.get(APP_FONT, getContext());
        setTypeface(tf);
    }
}