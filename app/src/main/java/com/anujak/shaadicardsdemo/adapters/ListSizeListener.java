package com.anujak.shaadicardsdemo.adapters;

public interface ListSizeListener {
    void onListCleared();
}
