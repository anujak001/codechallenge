package com.anujak.shaadicardsdemo.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.anujak.shaadicardsdemo.R;
import com.anujak.shaadicardsdemo.models.Result;
import com.anujak.shaadicardsdemo.widgets.AppTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MatchCardAdapter extends RecyclerView.Adapter {

    private ListSizeListener mListSizeListner;
    private List<Result> mList;
    private Context mContext;

    public MatchCardAdapter(Context context, List<Result> resultList, ListSizeListener listSizeListener) {
        mContext = context;
        mList = resultList;
        mListSizeListner = listSizeListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.layout_matchcard, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new MatchCardViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final MatchCardViewHolder matchCardViewHolder = (MatchCardViewHolder) viewHolder;
        Result result = mList.get(position);

        //Reset views
        matchCardViewHolder.itemView.setVisibility(View.VISIBLE);
        matchCardViewHolder.mLlOptnSelector.setVisibility(View.VISIBLE);
        matchCardViewHolder.mLlConnected.setVisibility(View.GONE);


        String name = result.getName().getFirst() + " " + result.getName().getLast();
        matchCardViewHolder.mTvMatchCardName.setText(name);

        String brief = result.getDob().getAge() + " Years, " + result.getGender() +
                "\n" + result.getLocation().getCity() + " " + result.getLocation().getState() + " - "
                + result.getNat();
        matchCardViewHolder.mTvMatchCardBrief.setText(brief);

        String thumLink = result.getPicture().getLarge();
        if (thumLink != null && !thumLink.isEmpty()) {
            Picasso piccassInst = Picasso.get();
            piccassInst.load(thumLink)
                    .placeholder(R.drawable.profile_pic_placeholder)
                    .error(R.drawable.profile_pic_placeholder)
                    .fit()
                    .noFade()
                    .into(matchCardViewHolder.mIvMatchCardProPic);
        } else {
            matchCardViewHolder.mIvMatchCardProPic.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.profile_pic_placeholder));
        }

        View.OnClickListener onDeclined = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.exit_left);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        matchCardViewHolder.itemView.setVisibility(View.GONE);
                        mList.remove(matchCardViewHolder.getAdapterPosition());
                        notifyItemRemoved(matchCardViewHolder.getAdapterPosition());
                        if (mList.isEmpty()) {
                            mListSizeListner.onListCleared();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                matchCardViewHolder.itemView.startAnimation(animation);
            }
        };

        View.OnClickListener onAccepted = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(matchCardViewHolder.mLlOptnSelector, "scaleX", 1f, 0f);
                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(matchCardViewHolder.mLlConnected, "scaleX", 0f, 1f);
                final ObjectAnimator oa3 = ObjectAnimator.ofFloat(matchCardViewHolder.mLlOptnSelector, "scaleX", 0f, 1f);
                oa1.setInterpolator(new DecelerateInterpolator());
                oa2.setInterpolator(new AccelerateDecelerateInterpolator());
                oa2.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.exit_right);
                                animation.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        matchCardViewHolder.itemView.setVisibility(View.GONE);
                                        mList.remove(matchCardViewHolder.getAdapterPosition());
                                        notifyItemRemoved(matchCardViewHolder.getAdapterPosition());
                                        if (mList.isEmpty()) {
                                            mListSizeListner.onListCleared();
                                        }
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                matchCardViewHolder.itemView.startAnimation(animation);
                            }
                        }, 300);
                    }
                });
                oa1.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        matchCardViewHolder.mLlOptnSelector.setVisibility(View.GONE);
                        matchCardViewHolder.mLlConnected.setVisibility(View.VISIBLE);
                        oa3.start();
                        oa2.start();
                    }
                });
                oa1.start();
            }
        };


        matchCardViewHolder.mIvMatchCardDecline.setOnClickListener(onDeclined);
        matchCardViewHolder.mIvMatchCardAcceptConnect.setOnClickListener(onAccepted);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}

class MatchCardViewHolder extends RecyclerView.ViewHolder {

    public final CircleImageView mIvMatchCardProPic;
    public final AppTextview mTvMatchCardName;
    public final AppTextview mTvMatchCardBrief;
    public final LinearLayout mIvMatchCardDecline;
    public final LinearLayout mIvMatchCardAcceptConnect;
    public final LinearLayout mLlOptnSelector;
    public final View mLlConnected;

    public MatchCardViewHolder(@NonNull View itemView) {
        super(itemView);
        mIvMatchCardProPic = itemView.findViewById(R.id.iv_matchcard_pro_pic);
        mTvMatchCardName = itemView.findViewById(R.id.tv_matchcard_name);
        mTvMatchCardBrief = itemView.findViewById(R.id.tv_matchcard_brief);
        mIvMatchCardDecline = itemView.findViewById(R.id.btn_matchcard_decline);
        mIvMatchCardAcceptConnect = itemView.findViewById(R.id.btn_matchcard_accept_connect);
        mLlOptnSelector = itemView.findViewById(R.id.ll_matchcard_optn_selector);
        mLlConnected = itemView.findViewById(R.id.ll_matchcard_connected);
    }
}
