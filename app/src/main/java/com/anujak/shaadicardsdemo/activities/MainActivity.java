package com.anujak.shaadicardsdemo.activities;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.anujak.shaadicardsdemo.R;
import com.anujak.shaadicardsdemo.adapters.ListSizeListener;
import com.anujak.shaadicardsdemo.adapters.MatchCardAdapter;
import com.anujak.shaadicardsdemo.models.RootResponse;
import com.anujak.shaadicardsdemo.models.Result;
import com.anujak.shaadicardsdemo.network.RetrofitAPI;
import com.anujak.shaadicardsdemo.network.RetrofitClient;
import com.anujak.shaadicardsdemo.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    private LinearLayout llProgressDialog;
    private RecyclerView rvMatchCards;
    private MatchCardAdapter matchCardAdapter;
    private List<Result> mList;
    private LinearLayout llEmptyListMsg;
    private LinearLayout llRecyclerView;
    private SwipeRefreshLayout swlRefreshList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        fetchRecord();
    }

    private void init() {
        swlRefreshList = findViewById(R.id.swrl_main_refresh);
        llProgressDialog = findViewById(R.id.ll_main_progress_container);
        llEmptyListMsg = findViewById(R.id.ll_main_container_empty_list_msg);
        llRecyclerView = findViewById(R.id.ll_main_container_match_cards);

        swlRefreshList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swlRefreshList.setRefreshing(false);
                fetchRecord();
            }
        });

        rvMatchCards = findViewById(R.id.rv_main_match_cards);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMatchCards.setLayoutManager(layoutManager);
        mList = new ArrayList<>();
        matchCardAdapter = new MatchCardAdapter(this, mList, new ListSizeListener() {
            @Override
            public void onListCleared() {
                Log.d("Main Actviity", "List Cleared");
                llRecyclerView.setVisibility(View.GONE);
                llEmptyListMsg.setVisibility(View.VISIBLE);
                llProgressDialog.setVisibility(View.GONE);
            }
        });
        rvMatchCards.setAdapter(matchCardAdapter);
    }

    private void fetchRecord() {
        llProgressDialog.setVisibility(View.VISIBLE);
        llRecyclerView.setVisibility(View.GONE);
        llEmptyListMsg.setVisibility(View.GONE);
        if (Utils.isNetworkAvailable(this)) {
            RetrofitAPI api = RetrofitClient.getInstance(this);
            Call<RootResponse> fetchRecordCall = api.fetchMemberRecords(10);
            fetchRecordCall.enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, retrofit2.Response<RootResponse> response) {
                    Log.e("RootResponse Code", String.valueOf(response.code()));
                    if (response.code() == 200) {
                        RootResponse rootResponse = response.body();
                        List<Result> tempList = rootResponse.getResults();
                        mList.clear();

                        mList.addAll(tempList);
                        llProgressDialog.setVisibility(View.GONE);
                        if (mList.size() > 0) {
                            llRecyclerView.setVisibility(View.VISIBLE);
                            llEmptyListMsg.setVisibility(View.GONE);
                        } else {
                            llRecyclerView.setVisibility(View.GONE);
                            llEmptyListMsg.setVisibility(View.VISIBLE);
                        }
                        matchCardAdapter.notifyDataSetChanged();
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    t.printStackTrace();
                    llProgressDialog.setVisibility(View.GONE);
                    llRecyclerView.setVisibility(View.GONE);
                    llEmptyListMsg.setVisibility(View.GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Error in loading data");
                    builder.setMessage("Wanna give it one more try?");
                    builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Log.i("Main Activity", "Retrying fetch");
                            fetchRecord();
                        }
                    });
                    builder.create().show();
                }
            });
        } else {
            llProgressDialog.setVisibility(View.GONE);
            llRecyclerView.setVisibility(View.GONE);
            llEmptyListMsg.setVisibility(View.GONE);
            final Snackbar snackbar = Snackbar
                    .make(getWindow().getDecorView(), "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    fetchRecord();
                }
            });
            snackbar.show();
        }
    }
}
