package com.anujak.shaadicardsdemo.network;

import com.anujak.shaadicardsdemo.models.RootResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitAPI {

    @GET("api/")
    Call<RootResponse> fetchMemberRecords(@Query("results") int recordPageSize);
}
